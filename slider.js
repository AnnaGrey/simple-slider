var slideIndex = 1;

var slideWidth = 700; 
var sliderList = document.querySelector('.slider__list');
var slides = document.querySelectorAll('.slider__item');
var previous = document.querySelector('#prev_slide');
var next = document.querySelector('#next_slide');
var pos = 0;

sliderList.style.width = slides.length * slideWidth + 'px';

previous.onclick = toPrev;
next.onclick = toNext;

function toPrev() {
    pos--;
  
    if (pos < 0) {
      var children = sliderList.children;
  
      sliderList.style.transition = null;
      sliderList.style.left = -(pos + 2) * slideWidth + 'px';
      sliderList.insertBefore(children[slides.length - 1], children[0]);
      pos++;
     }
    requestAnimationFrame(function(){ //ожидаем следующего запланированного reflow/repain;
      requestAnimationFrame(function(){ 
        //предыдущий reflow рассчитал новый dom элемент
        sliderList.style.transition = 'left 0.6s ease-in-out';
        sliderList.style.left = -(slideWidth * pos) + 'px';
      })
    });
  }

function toNext() {
  pos++;
  
  if (pos > slides.length -1) {
    var children = sliderList.children;

    sliderList.style.transition = null;
    sliderList.style.left = -(pos - 2) * slideWidth + 'px';
    sliderList.appendChild(children[0]);
    pos--;
    console.log(pos);
  }
  
  requestAnimationFrame(function(){ 
    requestAnimationFrame(function(){ 
      sliderList.style.transition = 'left 0.6s ease-in-out';
      sliderList.style.left = -(slideWidth * pos) + 'px';
    })
  }); 
}


function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var pics = document.querySelectorAll('.slider__item');
    var dots = document.getElementsByClassName("slider-dots_item");
    if (n > pics.length) {
      slideIndex = 1
    }
    if (n < 1) {
        slideIndex = pics.length
    }
    for (i = 0; i < pics.length; i++) {
        pics[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    pics[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
}

